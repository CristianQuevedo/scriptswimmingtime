# GET_TRIAL_NAME = r"((Fem|Masc).,\s*([0-9]\s[x]\s)*([0-9]+)m\s*(Libre|Espalda|Braza|Mariposa|Estilos))"
# TAKE_SWIMMERS_NAMES = r"['.'](\w+\s*\w+),\s([a-zA-Záéíóú]+)\s*(\d\d)\s*"
# GET_TIMES = r"((?:[0-9]:)*\d\d['.']\d\d)"

GET_TRIAL_NAME = r"(((4X)*([1-9]+0+))\s+(M['.']\s+)*(ESTILOS|LIBRE|ESPALDA|BRAZA|MARIPOSA))"
TAKE_SWIMMERS_NAMES = r"([a-zA-ZáéíóúñÑ]+)([0-9]{4})([a-zA-ZáéíóúñÑ]+\s*-*[a-zA-ZáéíóúñÑ]+\s*-*)((?:[a-zA-ZáéíóúñÑ]+\s*-*)+)"
GET_PRELIMINAR_TOTAL_TIMES = r"(?:PRELIMINAR)((?:[0-9]:)*\d\d['.']\d\d)"
GET_FINAL_TOTAL_TIMES = r"(?:FINAL)((?:[0-9]:)*\d\d['.']\d\d)"
GET_TOTAL_TIMES_FOR_LONG_TRIAL = r"(?:[0-9],[0-9])([0-9]{1,2}:\d\d['.']\d\d)"
GET_SECTION_TIMES = r"(?:(?:[0-9]:)*\d\d['.']\d\d)([0-9]+0+)((?:[0-9]+:)*\d\d['.']\d\d)"


TAKE_SWIMMERS_NAMES_RELEVOS = r"[0-9]{4}([a-zA-ZáéíóúñÑÁAÉÍÓÚÜÏ]+\s*-*[a-zA-ZáéíóúñÑÁAÉÍÓÚÜÏ]+\s*-*)((?:[a-zA-ZáéíóúñÑ]+\s*-*))"
TAKE_CLUB_AND_RESULT= r"(C(?:['.']*[])+(\s*[A-Z]+)+)[0-9]+,\d((?:[0-9]:)*\d\d['.']\d\d)"
TAKE_SECTION_RELEVOS = r"([1-9]5*0{1,2})((?:[0-9]:)*\d\d['.']\d\d)"

# TAKE_CLUB_AND_RESULT = r"((?:[A-Z]+\s)*(?:[A-Z]['.'])+\s*[A-Z]*)[0-9]+,\d((?:[0-9]:)*\d\d['.']\d\d)"
