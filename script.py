import PyPDF2
import xlwt
import re
import RegexPatterns
import sys

# Las secciones del preliminar no se pondran debido, a que hay tiempos que estan mal puestas

def getLengthTrial(text):
    return int(re.search(RegexPatterns.GET_TRIAL_NAME, text).group(3)) / 50

def getTrial(text):
    return re.findall(RegexPatterns.GET_TRIAL_NAME, text)

pdfFileObject = open('pdf/a.pdf', 'rb')
LENGTH_SWIMMING_POOL = 50
pdfReader = PyPDF2.PdfFileReader(pdfFileObject)
count = pdfReader.numPages
book = xlwt.Workbook()

def generate(page, swimmers, trialFinalTimes, trialPreliminaryTimes, trialSectionTimes,meters):
    sheet1 = book.add_sheet("Page" + str(page))
    cols = ["APELLIDOS", "NOMBRE", "AÑO", "CLUB", "TIEMPO"]
    sections = meters//100
    for num in range(len(swimmers)):
        num_column = 4
        row = sheet1.row(num)
        row.write(0, swimmers[num][2])
        row.write(1, swimmers[num][3])
        row.write(2, swimmers[num][1])
        row.write(3, swimmers[num][0])
        # row.write(3, swimmers[num][0])
        if num >= len(trialFinalTimes):
            #No va a la final
            row.write(num_column, 0)
            num_column +=1
        else:
            row.write(num_column, trialFinalTimes[num])
            for i in range(sections):
                num_column += 1
                row.write(num_column, trialSectionTimes[num*sections+i][1])
            num_column +=1

        if num >= len(trialPreliminaryTimes):
            #BAJA
            row.write(num_column, 0)
        else:
            row.write(num_column, trialPreliminaryTimes[num])
            # for n in range(sections):
            #     num_column += 1
            #     row.write(num_column, trialSectionTimes[num*sections+n][1])

    book.save("test.xls")

def generateRelevos(page, swimmers, trialClubTimes, trialSectionTimes,meters):
    sheet1 = book.add_sheet("Page" + str(page))
    sections = meters//LENGTH_SWIMMING_POOL

    for num in range(len(trialClubTimes)):
        num_column = 2
        row = sheet1.row(num)
        row.write(0, trialClubTimes[num][0])
        row.write(1, trialClubTimes[num][1])
        for i in range(4):
            row.write(num_column, swimmers[num * 4+i])
            num_column += 1
            for s in range(sections):
                row.write(num_column, trialSectionTimes[num*4*sections+s+(i*sections)][1])
                num_column += 1

    book.save("test.xls")

for i in range(count):
    page = pdfReader.getPage(i)
    result = page.extractText()
    array = result.replace("\n","")
    trial = getTrial(result)
    print(array)
    try:
        if trial[0][2] == "4X":
            meters = int(trial[0][3])
            if meters > 50:
                trialClubTimes = re.findall(RegexPatterns.TAKE_CLUB_AND_RESULT, array)
                trialSectionTimes = re.findall(RegexPatterns.TAKE_SECTION_RELEVOS, array)
                swimmers = re.findall(RegexPatterns.TAKE_SWIMMERS_NAMES_RELEVOS, array)
                generateRelevos(i, swimmers, trialClubTimes, trialSectionTimes,meters)

        else:
                meters = int(trial[0][1])
                if meters > 400:
                    trialTimes = re.findall(RegexPatterns.GET_TOTAL_TIMES_FOR_LONG_TRIAL, array)
                    trialSectionTimes = re.findall(RegexPatterns.GET_SECTION_TIMES, array)
                    swimmers = re.findall(RegexPatterns.TAKE_SWIMMERS_NAMES, array)
                    generate(i,swimmers,trialTimes,[],trialSectionTimes,meters)

                else:
                    if meters >= 200:
                        trialFinalTimes = re.findall(RegexPatterns.GET_FINAL_TOTAL_TIMES, array)
                        trialPreliminaryTimes = re.findall(RegexPatterns.GET_PRELIMINAR_TOTAL_TIMES, array)
                        trialSectionTimes = re.findall(RegexPatterns.GET_SECTION_TIMES, array)
                        swimmers = re.findall(RegexPatterns.TAKE_SWIMMERS_NAMES, array)
                        generate(i,swimmers,trialFinalTimes,trialPreliminaryTimes,trialSectionTimes,meters)
    except:
        sys.exit("No se encuentra más tiempos")



